;(function() {

  angular
    .module('boilerplate', [
      'nvd3'
    ]);


  angular
    .module('boilerplate')
    .run(run);

  run.$inject = ['$rootScope', 'QueryService'];

  function run($scope, QueryService) {

    // Call API
    QueryService.query('GET', '', {}, {})
      .then(function(response) {
        // Set data to nvd3 element in index.html
        $scope.dataOutput = response.data;
      });

      // Apply default options for display purposes
      $scope.options = {
            chart: {
                type: 'pieChart',
                height: 500,
                x: function(d){return d.key;},
                y: function(d){return d.y;},
                pie: {
                    dispatch: {
                        elementClick: function(e){ console.log(e.data.key) }
                    }
                },
                showLabels: false,
                duration: 500,
                labelThreshold: 0.01,
                labelSunbeamLayout: true,
                legend: {
                    margin: {
                        top: 5,
                        right: 35,
                        bottom: 5,
                        left: 0
                    }
                }
            }
        };

  }


})();