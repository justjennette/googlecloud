# README

Compile the files:
 $ gulp build

To run locally:
  $ dev_appserver.py --php_executable_path=/usr/local/bin/php-cgi ./_build

Deploy to the Google AppServer:
  $ gcloud app deploy -v 100

D3 Guide:
 https://krispo.github.io/angular-nvd3/