<?php

$projectId = "test-deploy-151700";
putenv('GOOGLE_APPLICATION_CREDENTIALS='.dirname(__FILE__) . '/key.json');

if( ! ini_get('date.timezone') )
{
    date_default_timezone_set('GMT');
}

require_once __DIR__.'/vendor/autoload.php';

use Google\Cloud\BigQuery\BigQueryClient;
use Google\Cloud\ServiceBuilder;

$query='SELECT Industry, total FROM (SELECT count(Industry) AS total FROM Segment.Contacts GROUP by Industry) ORDER BY total DESC LIMIT 10';

$builder = new ServiceBuilder(['projectId' => $projectId ]);

$bigQuery = $builder->bigQuery();

$job = $bigQuery->runQueryAsJob($query);
$info=$job->info();

$queryResults = $job->queryResults();

if ($queryResults->isComplete()) 
{
    $i = 0;
    $rows = $queryResults->rows();

    foreach ($rows as $row) 
    {
        $array = [];
        $array['key'] = $row['Industry'];
        $array['y'] = intval($row['total']);
        $result[] = $array;
    }
} 
else 
{
    throw new Exception('The query failed to complete');
}

echo json_encode($result);
